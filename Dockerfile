# syntax=docker/dockerfile:1
FROM node:16.13.0-alpine3.14

RUN apk add --no-cache g++ make python3 tzdata && \
    cp --verbose /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime && \
    echo 'America/Sao_Paulo' | tee /etc/timezone && \
    apk del tzdata
